.. pyevopt documentation master file, created by
   sphinx-quickstart on Sun Sep 24 11:17:13 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PyEvopt user guide
==================

**Pyevopt** is an open-source framework which offers what is necessary to optimize real functions with genetics algorithms and a wide range of operators.

.. note::
   This project is under active development.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
    index


Getting started
---------------
.. toctree::
  getting-started/what
  getting-started/quickstart

API Reference
-------------
.. toctree::
  api-reference/base
  api-reference/optimizer
  api-reference/genetic
  api-reference/early_stop
  api-reference/sampler
