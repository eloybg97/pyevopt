Genetic Algorithm
#################

.. automodule:: pyevopt.genetic

.. autoclass:: pyevopt.genetic.algorithm.GeneticAlgorithm
    :members:


Operators
*********

Selection Operators
===================

.. autoclass:: pyevopt.genetic.selection.SelectionTournament

.. autoclass:: pyevopt.genetic.selection.SelectionRouletteWheel

Crossover Operators
===================

.. autoclass:: pyevopt.genetic.crossover.KPointCrossover

.. autoclass:: pyevopt.genetic.crossover.UniformCrossover

Mutation Operators
==================
.. autoclass:: pyevopt.genetic.mutation.NormalMutation

.. autoclass:: pyevopt.genetic.mutation.FlipMutation

Survival Operators
==================
.. autoclass:: pyevopt.genetic.survival.BasicSurvival
