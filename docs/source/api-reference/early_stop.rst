Early Stop Criteria
###################
.. automodule:: pyevopt.early_stop

Max Evaluation Criteria
=======================

.. autoclass:: pyevopt.early_stop.MaxEvaluationCriteria
   :members:

Max Iteration Criteria
=======================

.. autoclass:: pyevopt.early_stop.MaxIterationCriteria
   :members:
