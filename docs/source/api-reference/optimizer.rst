Optimizer
#########

.. automodule:: pyevopt.optimizer

.. autoclass:: pyevopt.optimizer.Optimizer
   :members:
