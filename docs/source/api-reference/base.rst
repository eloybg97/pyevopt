Base Module
===========

.. automodule:: pyevopt.base

Algorithm
=========
.. autoclass:: pyevopt.base.Algorithm
   :members:

EarlyStopCriteria
=================
.. autoclass:: pyevopt.base.EarlyStopCriteria
   :members:

Sampler
=======
.. autoclass:: pyevopt.base.Sampler
   :members:

Population
==========
.. autoclass:: pyevopt.base.Population
   :members:
