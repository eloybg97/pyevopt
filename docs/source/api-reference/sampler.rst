Sampler
#######
.. automodule:: pyevopt.sampler

RealSampler
===========

.. autoclass:: pyevopt.sampler.RealSampler
   :members:
