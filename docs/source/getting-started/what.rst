What is PyEvopt?
================
PyEvopt is a package for evolutionary optimization in Python.
It is a Python library that provides an optimizer object, various
evolutionary algorithms and multiple operators that ables you to
customize your own algorithm.

At the core of the PyEvopt package, is the 
:py:class:`pyevopt.optimizer.Optimizer` object. With it, the user
can define how the problem will be resolved. 


Installing PyEvopt
==================
The only prerequisite for installing PyEvopt is Python itself.
To install it, use pip as follows:

.. code-block:: console

  (.venv) $ pip install pyevopt --index-url https://gitlab.com/api/v4/projects/50659038/packages/pypi/simple


About dependencies
------------------
PyEvopt depends on Numpy (v1.26.4) package but users do not have
to worry about installing it. PyEvopt also needs Python 3.9 to
3.12 to work.
