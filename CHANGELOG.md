# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0-alpha.1]
### Added
- Add documentation with Sphinx
- Test using [CEC2019 Benchmark Suite](https://github.com/P-N-Suganthan/CEC2019/blob/master/Definitions%20of%20%20CEC2019%20Benchmark%20Sute.pdf)
- Added new type of algorithm: Genetic Algorithm..
- Added new crossover operastors for Genetic Algorithms: K-Point Crossover and Uniform Crossover.
- Added new mutation operators for Genetic Algorithms: Flip Mutation and Normal Mutation.
- Added new selection operators for Genetic Algorithms: Tournament Selection and Rulette Wheel Selection.
- Added new survival operator for Genetic Algorithm: Basic Survival.

## [0.1.0-alpha.2]
### Added
- Added algorithm interface to allow to create new algorithms usable from optimizer.
- Added new early stop citeria based in the numbers of evaluations.

### Changed
- Change package structure. Now there is a new module called "genetic" with genetic algorithm and operators.
- Population's initialization is not algorithm's responsability. Now it is responsability of the problem itself.
- Change how to manage early stop criteria.

## [0.1.0-alpha.3]

### Fixed
- Reset stop criteria value each time optimization process is called. It lets to rerun optimization process whithout redefine stop criteria object.

### Added
- Added new function to benchmark test: Storn's Chebyshev Polynomial Fitting Problem, Griewank's funcction, Happy Cat Function and Ackley Function.
- Added a logger to track the optimization process.
- Added Population class.

### Changed
- Change package structure avoiding one class per file.
