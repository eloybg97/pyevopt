from pyevopt.base import Algorithm, Population
import numpy as np
import unittest


class AlgorithmTestCase(unittest.TestCase):
    def test1(self):
        alg = Algorithm()

        pop = np.random.rand(10, 5)

        population = Population(pop, (0, 1))
        population.evaluate(np.sum)

        with self.assertRaises(NotImplementedError):
            alg.next(population, np.sum)
