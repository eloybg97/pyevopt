import numpy as np
import unittest

from pyevopt.early_stop import (
        MaxIterationCriteria,
        MaxEvaluationCriteria
    )
from pyevopt.optimizer import Optimizer
from pyevopt.genetic.algorithm import GeneticAlgorithm
from pyevopt.sampler import RealSampler
from pyevopt.genetic.selection import SelectionTournament
from pyevopt.genetic.crossover import UniformCrossover
from pyevopt.genetic.mutation import NormalMutation
from pyevopt.genetic.survival import BasicSurvival
from .benchmark import (
        rastrigin_function,
        storn_chebyshev_polynomial,
        griewank_function,
        happy_cat_function,
        ackley_function
    )


class TestOptimizer(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        selection = SelectionTournament(3)
        crossover = UniformCrossover(0.7)
        mutation = NormalMutation(0.01)
        survival = BasicSurvival()

        alg = GeneticAlgorithm(selection, crossover, mutation, survival)
        cls.opt = Optimizer(alg)

    def test_minimize_f1(self):
        early_stop = MaxIterationCriteria(max_iter=10)
        sampler = RealSampler()

        y = self.opt.run(
                storn_chebyshev_polynomial, 9, (-8192, 8192),
                sampler, early_stop, pop_size=60)

        self.common_assert(y, (60, 9), (60, ), (-8192, 8192))

    def test_maximize_f4(self):
        early_stop = MaxIterationCriteria(max_iter=10)
        sampler = RealSampler()

        y = self.opt.run(
                rastrigin_function, 10, (-100, 100),
                sampler, early_stop, pop_size=50)

        self.common_assert(y, (50, 10), (50, ), (-100, 100))

    def test_minimize_f5(self):
        early_stop = MaxIterationCriteria(max_iter=10)
        sampler = RealSampler()

        y = self.opt.run(
                griewank_function, 9, (-100, 100),
                sampler, early_stop, pop_size=60)

        self.common_assert(y, (60, 9), (60, ), (-100, 100))

    def test_maximize_f9(self):
        early_stop = MaxIterationCriteria(max_iter=10)
        sampler = RealSampler()

        y = self.opt.run(
                happy_cat_function, 9, (-100, 100),
                sampler, early_stop, pop_size=60)

        self.common_assert(y, (60, 9), (60, ), (-100, 100))

    def test_maximize_f10(self):
        early_stop = MaxIterationCriteria(max_iter=10)
        sampler = RealSampler()

        y = self.opt.run(
                ackley_function, 9, (-100, 100),
                sampler, early_stop, pop_size=60)

        self.common_assert(y, (60, 9), (60, ), (-100, 100))

    def common_assert(self, res, pop_shape, fit_shape, domain):
        better = res["best"]
        self.assertEqual(res["P"].shape, pop_shape)
        self.assertEqual(res["F"].shape, fit_shape)
        self.assertGreater(res["F"][better], res["F"].mean())
        self.assertGreater(np.std(res["P"]), 0)
        self.assertTrue((res["P"] <= domain[1]).all())
        self.assertTrue((res["P"] >= domain[0]).all())
