import numpy as np
import unittest
from pyevopt.genetic.crossover import (UniformCrossover,
                                       KPointCrossover)


class TestCrossoverKPoint(unittest.TestCase):
    def test_kpoint_1_all(self):
        size = 20
        n_var = 5
        crossover = KPointCrossover(k=1, p=1)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 10))

        C = crossover.do(P, parent_idx)

        assert C.shape == (20, 5)
        assert np.logical_or(
            (P[parent_idx[0]] == C[:10, :]).all(axis=0),
            (P[parent_idx[0]] == C[10:, :]).all(axis=0),
        ).all()
        assert np.logical_xor(
            (P[parent_idx[1]] == C[:10, :]).all(axis=0),
            (P[parent_idx[1]] == C[10:, :]).all(axis=0),
        ).all()

    def test_kpoint_1_none(self):
        size = 20
        n_var = 5
        crossover = KPointCrossover(k=1, p=0)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 10))

        C = crossover.do(P, parent_idx)
        Ctest = np.reshape(C, (2, 10, 5))

        assert C.shape == (20, 5)
        assert (P[parent_idx] == Ctest).all()

    def test_kpoint_1_some(self):
        size = 20
        n_var = 5
        crossover = KPointCrossover(k=1, p=0.5)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 10))

        C = crossover.do(P, parent_idx)
        Ctest = np.reshape(C, (2, 10, 5))

        assert C.shape == (20, 5)
        assert np.logical_or(
            (P[parent_idx[0]] == C[:10, :]).all(axis=0),
            (P[parent_idx[1]] == C[:10, :]).all(axis=0),
        ).any()

        assert (np.in1d(C, np.reshape(P[parent_idx], (size, n_var)))).any()


class TestCrossoverUniform(unittest.TestCase):
    def test_uniform_all(self):
        size = 20
        n_var = 5
        crossover = UniformCrossover(p=1)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 20))

        C = crossover.do(P, parent_idx)

        assert C.shape == (20, 5)
        assert (P[parent_idx] == C).any(axis=(0, 1)).all()

    def test_uniform_none(self):
        size = 20
        n_var = 5
        crossover = UniformCrossover(p=0)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 20))

        C = crossover.do(P, parent_idx)

        assert C.shape == (20, 5)
        assert (P[parent_idx, :][0] == C).all()

    def test_uniform_some(self):
        size = 20
        n_var = 5
        crossover = UniformCrossover(p=0.5)

        P = np.random.rand(size, n_var)
        parent_idx = np.random.randint(0, 20, size=(2, 20))

        C = crossover.do(P, parent_idx)

        assert C.shape == (20, 5)
        assert (P[parent_idx] == C).any(axis=(0, 1)).all()
        assert (np.in1d(C, np.reshape(P[parent_idx], (size * 2, n_var)))).any()
