import numpy as np
import unittest
from pyevopt.genetic.mutation import NormalMutation
from pyevopt.genetic.mutation import FlipMutation


class TestFlipMutation(unittest.TestCase):
    def test_flip_all(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = FlipMutation(p=1)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert ((P != M).sum(axis=1) == 2).all()

    def test_flip_none(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = FlipMutation(p=0)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert (M == P).all()

    def test_flip_some(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = FlipMutation(p=0.5)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert ((P != M).sum(axis=1) == 2).any()


class TestNormalMutation(unittest.TestCase):
    def test_normal_all(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = NormalMutation(mu=0, sigma=0.1, p=1)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert (P != M).any(axis=1).all()

    def test_normal_none(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = NormalMutation(mu=0, sigma=0.1, p=0)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert (P == M).all()

    def test_normal_some(self):
        np.random.seed(0)

        size = 20
        n_var = 5
        mutation = NormalMutation(mu=0, sigma=0.1, p=0.5)

        P = np.random.rand(size, n_var)

        M = mutation.do(P)

        assert M.shape == P.shape
        assert (P != M).any(axis=1).any()
        assert (P == M).all(axis=1).any()
