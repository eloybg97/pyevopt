import numpy as np
import unittest
from pyevopt.genetic.selection import SelectionRouletteWheel
from pyevopt.genetic.selection import SelectionTournament


class TestSelection(unittest.TestCase):
    def initialize_population(self, size, n_var):
        self.P = np.random.rand(size, n_var)
        self.F = np.arange(size)

    def common_assert(self, selected_idx, to_select):
        assert selected_idx.shape == (2, to_select // 2)
        assert np.mean(self.F[selected_idx]) > np.min(self.F)


class TestSelectionRouletteWheel(TestSelection):
    def setUp(self):
        self.select = SelectionRouletteWheel()

    def test_roulette_wheel_less_than_total(self):
        size = 60
        n_var = 5
        to_select = 30

        self.initialize_population(size, n_var)
        selected_idx = self.select.do(self.P, self.F, to_select)

        self.common_assert(selected_idx, to_select)

    def test_roulette_wheel_more_than_total(self):
        size = 50
        n_var = 2
        to_select = 100

        self.initialize_population(size, n_var)
        selected_idx = self.select.do(self.P, self.F, to_select)

        unique_values, counts = np.unique(
                self.F[selected_idx], return_counts=True)
        idx_max_count = np.argmax(counts)
        idx_min_count = np.argmin(counts)

        self.common_assert(selected_idx, to_select)
        assert unique_values[idx_max_count] > unique_values[idx_min_count]

    def test_roulette_wheel_prob_zero(self):
        size = 2
        n_var = 2
        to_select = 20

        self.initialize_population(size, n_var)
        selected_idx = self.select.do(self.P, self.F, to_select)

        unique_values, counts = np.unique(
                self.F[selected_idx], return_counts=True)
        idx_max_count = np.argmax(counts)
        idx_min_count = np.argmin(counts)

        self.common_assert(selected_idx, to_select)
        assert unique_values[idx_max_count] == unique_values[idx_min_count]

    def test_roulette_wheel_minimum(self):
        size = 200
        n_var = 5
        to_select = 2

        self.initialize_population(size, n_var)
        selected_idx = self.select.do(self.P, self.F, to_select)

        self.common_assert(selected_idx, to_select)


class TestSelectionTournament:
    def test_tournament_less_than_total(self):
        size = 20
        n_var = 5
        to_select = 10

        P = np.random.rand(size, n_var)
        F = np.random.rand(size)

        select = SelectionTournament(3)
        selected_idx = select.do(P, F, to_select)

        assert selected_idx.shape == (2, to_select // 2)
        assert np.min(F[selected_idx]) > np.min(F)

    def test_tournament_more_tah_total(self):
        size = 20
        n_var = 5
        to_select = 40

        P = np.random.rand(size, n_var)
        F = np.random.rand(size)

        select = SelectionTournament(3)
        selected_idx = select.do(P, F, to_select)

        assert selected_idx.shape == (2, to_select // 2)
        assert np.min(F[selected_idx]) > np.min(F)

    def test_tournament_minimum(self):
        size = 20
        n_var = 5
        to_select = 2

        P = np.random.rand(size, n_var)
        F = np.random.rand(size)

        select = SelectionTournament(20)
        selected_idx = select.do(P, F, to_select)

        assert selected_idx.shape == (2, to_select // 2)
        assert np.min(F[selected_idx]) > np.min(F)
