import numpy as np
import unittest
from pyevopt.genetic.survival import BasicSurvival
from pyevopt.base import Population


class TestBasicSurvival(unittest.TestCase):
    def test_basic_survival(self):
        P = np.random.rand(20, 5)
        newP = np.random.rand(20, 5)

        population = Population(P, (0, 1))
        new_population = Population(newP, (0, 1))

        population.evaluate(np.sum)
        new_population.evaluate(np.sum)

        op = BasicSurvival()

        off = op.do(population, new_population)

        self.assertEqual(off.fitness.shape, new_population.fitness.shape)
        self.assertEqual(off.fitness.shape, new_population.fitness.shape)
        self.assertTrue((off.population == new_population.population).all())
        self.assertTrue((off.fitness == new_population.fitness).all())
