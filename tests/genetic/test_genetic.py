import numpy as np
import unittest

from pyevopt.genetic.algorithm import GeneticAlgorithm
from pyevopt.base import Population

from pyevopt.genetic.selection import (
        SelectionTournament,
        SelectionRouletteWheel
)
from pyevopt.genetic.crossover import (
        UniformCrossover,
        KPointCrossover
)
from pyevopt.genetic.mutation import (
        NormalMutation,
        FlipMutation
)
from pyevopt.genetic.survival import BasicSurvival
from ..benchmark import rastrigin_function


class TestGeneticAlgorithm(unittest.TestCase):
    def setUp(self):
        P = np.random.randint(-100, 100, size=(60, 9))
        self.population = Population(P, (-100, 100))
        self.population.evaluate(rastrigin_function)

    def test_genetic_1(self):
        selection = SelectionTournament()
        crossover = UniformCrossover(0.7)
        mutation = NormalMutation(0.01)
        survival = BasicSurvival()

        alg = GeneticAlgorithm(selection, crossover, mutation, survival)

        hist, population = self.run_algorithm_loop(alg)
        self.common_assert(hist, population)

    def test_genetic_2(self):
        selection = SelectionRouletteWheel()
        crossover = KPointCrossover(k=1, p=0.7)
        mutation = FlipMutation(0.01)
        survival = BasicSurvival()

        alg = GeneticAlgorithm(selection, crossover, mutation, survival)

        hist, population = self.run_algorithm_loop(alg)
        self.common_assert(hist, population)

    def run_algorithm_loop(self, algorithm):
        hist = []
        hist.append(np.mean(self.population.fitness))

        for _ in range(10):
            population, _ = algorithm.next(self.population, rastrigin_function)
            hist.append(np.mean(population.fitness))

        return hist, population

    def common_assert(self, hist, population):
        P = population.population
        F = population.fitness

        better = np.argmax(F)
        X = np.vstack([np.arange(len(hist)), np.ones(len(hist))]).T

        assert F.shape == (60,)
        assert P.shape == (60, 9)
        assert F[better] > np.mean(F)
        assert np.linalg.lstsq(X, hist, rcond=None)[0][0] > 0
        assert np.std(P) > 0
        assert (P <= 100).all()
        assert (P >= -100).all()
