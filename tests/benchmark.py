import numpy as np
import numpy.typing as npt


def rastrigin_function(X: npt.ArrayLike) -> float:
    x = np.array(X)
    return np.sum(x * x - 10 * np.cos(2 * np.pi * x) + 10)


def storn_chebyshev_polynomial(X: npt.ArrayLike) -> float:
    x = np.array(X)

    u = 0
    D = np.arange(x.size) + 1
    e = np.flip(D) - 1

    u = np.sum(x * 1.2 ** e)
    v = np.sum(x * - 1.2 ** e)

    m = 32 * x.size

    # çThis value is for dimension 9
    d = 72.661

    p1 = (u - d) * (u - d) if (u < d) else 0
    p2 = (v - d) * (v - d) if (v < d) else 0

    p3 = 0

    for k in range(m):
        value = np.sum(x * ((2*k/m) - 1) ** e)

        if (value > 1):
            p3 += (value - 1)*(value - 1)

        elif (value < 1):
            p3 += (value + 1)*(value + 1)

    return p1 + p2 + p3


def griewank_function(X: npt.ArrayLike) -> float:
    x = np.array(X)
    i = np.arange(X.size) + 1

    return np.sum((x * x) / 4000) - np.prod(np.cos(x/np.sqrt(i)) + 1)


def happy_cat_function(X: npt.ArrayLike) -> float:
    x = np.array(X)
    D = x.size

    u = np.abs(np.sum(x * x - D))**0.25
    v = (0.5 * np.sum(x) + np.sum(x * x))/D
    return u + v + 0.5


def ackley_function(X: npt.ArrayLike) -> float:
    x = np.array(X)
    D = x.size

    u = -20 * np.exp(0.2 * np.sqrt((1/D) * np.sum(x * x)))
    v = np.exp((1/D) * np.sum(np.cos(2 * np.pi * x)))

    return u + v + 20 + np.e
