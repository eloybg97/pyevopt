import numpy as np
import unittest
from pyevopt.early_stop import (
        MaxIterationCriteria,
        MaxEvaluationCriteria
    )


class EarlyStopTestCase(unittest.TestCase):
    def test_max_iteration_criteria(self):
        stop_criteria = MaxIterationCriteria(25)
        self.run_iterations(stop_criteria)

        self.assertEqual(stop_criteria.value, 25)

    def test_max_iteration_criteria_reinit(self):
        stop_criteria = MaxIterationCriteria(25)
        self.run_iterations(stop_criteria)
        stop_criteria.init()

        self.assertEqual(stop_criteria.value, 0)

    def test_max_evaluation_criteria(self):
        stop_criteria = MaxIterationCriteria(200)
        self.run_iterations(stop_criteria)

        self.assertEqual(stop_criteria.value, 200)

    def run_iterations(self, stop_criteria):
        stop_criteria.init()

        while not stop_criteria.check():
            stop_criteria.next(evaluation=10)
