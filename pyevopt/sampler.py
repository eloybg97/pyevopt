"""
This module provides various ways to initialize the population for
evolutionary algorithms.
"""
import numpy as np
from .base import (Sampler, Population)


class RealSampler(Sampler):
    """
    Initializes a population with real-valued solutions.

    This sampler can be used to initialize the population of an evolutionary
    algorithm with real-valued solutions.

    The population is initialized by sampling from a uniform distribution.
    This allows for efficient initialization of large populations, and
    provides a good starting point for further evolution.

    """
    def __init__(self):
        pass

    def generate(self, size: int, dimension: int, domain: tuple[float, float]):
        """
        Generate a real-valued population of individuals.

        Generates a population of `size` individuals, each individual being
        an array of length `dimension`, with values chosen from the specified
        `domain`.

        Parameters
        ----------
        size: int
            The number of individuals in the population.
        dimension: int
            The size of each individual.
        domain
            A function that maps a value to a random sample from this domain.

        Returns
        -------
        np.ndarray
            An array of shape `(size, dimension)` containing the generated
            population.
        """
        population = np.random.rand(size, dimension)
        population = (population - domain[0]) / (domain[1] - domain[0])
        return Population(population, domain)
