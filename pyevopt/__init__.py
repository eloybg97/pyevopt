"""
This package provides a easy and flexible interface to allow users to use
and explore various evolutionary algorithms.

Description
-----------

The Evolutionary Algorithm Package (EAP) is designed to make it easy for
users to implement, run, and compare different evolutionary algorithms.
The package provides a set of classes and functions that can be used to
define and execute evolutionary algorithms.

Main Features
--------------

* **Easy to Use**: EAP has a simple and intuitive API that makes it easy
for users to define and run their own evolutionary algorithms.
* **Flexible**: Users can customize the behavior of their algorithms by
specifying parameters.
* **Supports Multiple Algorithms**: EAP currently supports genetic algorithm.
"""
from . import genetic, optimizer, base

__all__ = ["genetic", "optimizer", "base"]
