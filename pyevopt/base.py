"""
This module provides a suite of reusable components for building and
implementing evolutionary algorithms. It is designed to facilitate
the development of novel optimization strategies by providing a
foundation of common classes and interfaces.
"""
import numpy as np


class Algorithm:
    """
    The Algorithm class is an abstract base class that defines the basic
    interface for all evolutionary algorithms.

    This class is designed to help developers create customized algorithms
    logic for their evolutionary algorithms. By implementing this class,
    users can define new algorithms to use in the optimization process.
    """
    def __init__(self):
        self._objectsve = None
        self._domain = None

    def next(self, population, objective):
        """
        This method applies the optimization strategy defined by this
        algorithm to the given population, using the provided objective
        function as the fitness metric.

        Parameters
        ----------
        population: Population
            Represent the current generation of candidate solutions.

        objective: Callable
            A callable function that evaluates the quality of each
            individual in the population.

        Returns
        -------
        pyevopt.base.Population:
            The new population of candidate solutions after a single
            iteration of the genetic algorithm.
        dict:
            A dictionary containing information about the current iteration.

        Raises
        ------
        NotImplementedError
            If the method is not implemented in a subclass.

        Notes
        -----
        - This method is intended to be called repeatedly to iterate
          through the optimization process.
        - The specific behavior of this method depends on the concrete
          subclass and its implementation of the evolutionary algorithm.
        - It is assumed that the `objective` function is deterministic,
          meaning it always returns the same output for a given input.
        - This method is abstract and should be implemented in subclasses.
          The exact behavior depends on the specific algorithm used by the
          subclass.

        """
        raise NotImplementedError


class EarlyStopCriteria:
    """
    This class provides a way to control the optimization process by
    determining when to stop prematurely based on certain conditions.

    This class is designed to help developers create customized early
    stopping logic for their evolutionary algorithms. By implementing
    this class, users can define specific criteria for terminating the
    optimization process.

    Attributes
    ----------
    threshold: float
        Boundary value that, when reached or exceeded, signals to stop
        the optimization process prematurely.

    init_value: float
        Starting point of the optimization process. It represents
        the first value that will be evaluated by the algorithm.

    value: float
        Represents the latest evaluation result produced by the
        optimization process. This attribute serves as a reference
        point for monitoring progress and detecting convergence or
        exceeding the maximum threshold.
    """

    def __init__(self, threshold, init_value):
        self.max_threshold = threshold
        self.init_value = init_value
        self.value = init_value

    def init(self):
        """
        Initializes the EarlyStopCriteria object by resetting the
        current value (`value`) to the initial value (`init_value`).
        This sets the starting point for evaluating the optimization
        process.
        """
        self.value = self.init_value

    def next(self, **kwargs) -> None:
        """
        Update the current value for the next iteration of the
        optimization process.

        Parameters
        ----------
        **kwargs: dict, optional
            evaluation: Number of objective function evaluation that
            are executed by the algorithm.

        Raises
        ------
        NotImplementedError
            If the method is not implemented in a subclass.

        Notes
        -----
        - This method is abstract and should be implemented in subclasses.
          The exact behavior depends on the specific algorithm used by the
          subclass.

        """
        raise NotImplementedError

    def check(self) -> bool:
        """
        Check the condition to decide if a new iteration of the optimization
        process should be executed.

        Return
        ------
        bool:
            True if stop criteria is meet, False otherwise.

        Raises
        ------
        NotImplementedError
            If the method is not implemented in a subclass.

        Notes
        -----
        - This method is abstract and should be implemented in subclasses.
          The exact behavior depends on the specific algorithm used by the
          subclass.

        """
        raise NotImplementedError


class Sampler:
    """
    This class provides a way to generate new populations.

    This class is designed to help developers create customized samplers
    logic to generate new populations. By implementing this class, users
    can define new ways to generate it.
    """
    def __init__(self):
        pass

    def generate(self, size: int, dimension: int, domain):
        """
        Generate a population of individuals.

        Generates a population of `size` individuals, each individual being
        an array of length `dimension`, with values chosen from the specified
        `domain`.

        Parameters
        ----------
        size: int
            The number of individuals in the population.
        dimension: int
            The size of each individual.
        domain
            A function that maps a value to a random sample from this domain.

        Returns
        -------
        np.ndarray
            An array of shape `(size, dimension)` containing the generated
            population.

        Raises
        ------
        NotImplementedError
            If the method is not implemented in a subclass.

        Notes
        -----
        - This method is abstract and should be implemented in subclasses.
          The exact behavior depends on the specific algorithm used by the
          subclass.

        """
        raise NotImplementedError


class Population:
    """
    Class representing a population of individuals.

    This class holds the information about a population of individuals. It
    stores the actual population, its fitness values, number of individuals,
    dimension of each individual and domain from which the individuals were
    generated.

    Attributes
    ----------
    population: np.ndarray
        The array containing the population.
    fitness: np.ndarray
        The array containing the fitness values for each individual.
    n: int
        The number of individuals in the population.
    dimension: int
        The size of each individual.
    domain:
        A function that maps a value to a random sample from this domain.
    """
    def __init__(self, population, domain):
        self.population = population
        self.fitness = None
        self.n, self.dimension = population.shape
        self.domain = domain

    def _clip(self):
        np.clip(self.population, self.domain[0], self.domain[1],
                out=self.population)

    def evaluate(self, objective):
        """
        Evaluate each individual in the population using the provided
        objective function.

        This method calculates the fitness value for each individual in
        the population by applying the provided objective function to
        each individual. The results are stored in the `fitness` attribute
        of this object.

        Parameters
        ----------
        objective: callable
            A function that takes an individual and returns its fitness value.

        Notes
        -----
        - After calling this method, you can access the fitness values for each
          individual using the `fitness` attribute.

        """
        self._clip()
        self.fitness = np.apply_along_axis(objective, 1, self.population)
