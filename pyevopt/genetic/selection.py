"""
This module provides various selection operators that are commonly used
in genetic algorithms.

These operators can be used to pick some individuals from population.
"""
import logging
import numpy as np


class Selection:
    """
    Abstract base class for all selection operators in an evolutionary
    algorithm.

    This abstract class provides a general framework for creating new
    selection operators. Users can subclass this class and implement 
    their own specific selection strategy to select individuals for the
    next generation.
    """

    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def do(self, pop: np.array, fit: np.array, to_select=2) -> np.array:
        """
        Apply the selection operator to the given population.

        Parameters
        ----------
        pop: np.array
            The population to select from

        fit: np.array
            Fitness values of each individual.

        to_select: int
            Nº of individuals will be selected.

        Return
        ------
        np.array
            2-D array.
            1D: First or second parents
            2D: Index of each individual on the population (P)
        """
        selected_index = self._do(pop, fit, to_select=to_select)
        self.logger.debug("%d parents are selected.", selected_index.size)

        return selected_index

    def _do(self, pop: np.array, fit: np.array, to_select=2) -> np.array:
        """
        Specific procedure for each operator. To overriden method.

        Parameters
        ----------
        pop: np.array
            The population to select from

        fit: np.array
            Fitness values of each individuals.

        to_select: int
            Nº of individuals will be selected

        Return
        ------
        np.array
            2-D array.
            1D: First or second parent.
            2D: Index of each individual on the population.
        """
        raise NotImplementedError


class SelectionRouletteWheel(Selection):
    """
    Roulette wheel selection operator for evolutionary algorithms.

    This class implements the roulette wheel selection strategy, which
    selects individuals based on their fitness. The fitter an individual
    is, the higher the probability of being selected.
    """

    def __init__(self) -> None:
        super().__init__()

    def _do(self, pop: np.array, fit: np.array,
            to_select: int = 2) -> np.array:
        """
        Apply the roulette wheel selection operator to the given population.

        This method calculates the cumulative probability of each individual,
        selects an index randomly based on these probabilities, and returns
        the corresponding individual. The fitness is used to determine the
        probability of each individual being selected.

        Parameters
        ----------
        pop: np.array
            The population to select from

        fit: np.array
            Fitness values of each individuals.

        to_select: int
            Nº of individuals will be selected

        Return
        ------
        np.array
            2-D array.
            1D: First or second parent.
            2D: Index of each individual on the population.
        """
        prob = fit / fit.sum()
        possible_parents = np.arange(fit.size)
        selected_idx = np.random.choice(
            possible_parents, to_select, replace=True, p=prob
        )

        return np.reshape(selected_idx, (2, to_select // 2))

    def __str__(self):
        return f"{self.__class__.__name__}()"


class SelectionTournament(Selection):
    """
    Tournament selection operator for evolutionary algorithms.

    This class implements the tournament selection strategy, which selects
    the fittest individual from a random sample of individuals. The size
    of the tournament is specified by the user.
    
    Attributes
    ----------
    k: int
        Nº of individuals participate in each tournament.
    """

    def __init__(self, k: int = 3):
        self._k = k
        super().__init__()

    def _do(self, pop: np.array, fit: np.array,
            to_select: int = 2) -> np.array:
        """
        Apply the tournament selection operator to the given population.

        This method randomly selects individuals from the input population,
        forms a tournament of size `tournament_size`, and returns the fittest
        individual in the tournament.

        Parameters
        ----------
        pop: np.array
            The population to select from

        fit: np.array
            Fitness values of each individuals.

        to_select: int
            Nº of individuals will be selected

        Return
        ------
        np.array
            2-D array.
            1D: First or second parent.
            2D: Index of each individual on the population.
        """
        idx = np.random.randint(0, fit.size, size=(to_select, self._k))
        best_idx = np.apply_along_axis(np.argmax, 1, fit[idx])
        selected_idx = idx[np.arange(len(idx)), best_idx]

        return np.reshape(selected_idx, (2, selected_idx.size // 2))

    def __str__(self):
        return f"{self.__class__.__name__}(k = {self._k})"
