import numpy as np
from ..base import Population


class Survival:
    """
    Class to represent a survival operator.
    """

    def do(self, population, off):
        """
        Do survival operator.

        Parameters
        ----------
        pop: np.array
            Current population
        fit: np.array
            Current fitness values
        new_pop: np.array
            New generation population
        new_fit: np.array
            New generation fitness value

        Return
        ------
        tuple[np.array, np,.array]
            Set of individuals survivors and its fitness values.
        """
        survival_pop = np.append(population.population, off.population, axis=0)
        survival_fit = np.append(population.fitness, off.fitness, axis=0)

        idx = self._do(survival_fit)
        new = Population(survival_pop[idx, :], population.domain)
        new.fitness = survival_fit[idx]
        return new

    def _do(self, survival_fit: np.array):
        """
        Specific survival operator procedure. To overriden method.

        Parameters
        ----------
        survival_fit: np.array
            Fitness values.
        """
        raise NotImplementedError


class BasicSurvival(Survival):
    """
    Class to represent a Basic Survival operator.
    """

    def _do(self, survival_fit: np.array):
        """
        Survive .

        Parameters
        ----------
        survivalF: np.array
            Fitness values.

        Return
        ------
        np.array
            Index of individuals that will survive.
        """
        start = survival_fit.size // 2

        return np.arange(survival_fit.size) >= start
