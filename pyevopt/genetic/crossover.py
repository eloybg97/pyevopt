"""
This module provides various crossover operators used in genetic algorithms.

These operators can be used to combine two parent solutions into one or more
offspring, creating new candidate solutions for the next generation.
"""
import logging
import numpy as np


class Crossover:
    """
    Abstract base class for all crossover operators in an evolutionary
    algorithm.

    This abstract class provides a general framework for creating new
    crossover operators. Users can subclass this class and implement
    their own specific crossover strategy to combine parent individuals
    into child individuals.

    Attributes
    ----------
    input: int
        Nº parents required to each cross.
    output: int
        Nº childs produced on each cross.
    """

    def __init__(self, _input: int, _output: int, p: float = 1):
        self.input = _input
        self.output = _output
        self._p = p
        self.logger = logging.getLogger(__name__)

    def do(self, pop: np.array, parents_idx: np.array) -> np.array:
        """Perform the crosover procedure.

        From `pop` will choose as parents those referred to in `parents_idx`.
        It will apply the crossover procedure with the `prob` probability.

        Parameters
        ----------
        pop: np.array
            All population.

        parents_index: np.array
            2-D array. It contains the individuals's indices acting as parents.

        Return
        ------
        np.array
            Array of resulting population

        See Also
        --------
        pyevopt.operators.crossover.UniformCrossover
        pyevopt.operators.crossover.KPointCrossover
        """

        noff = int(parents_idx.size * (self.output / self.input))

        # Crossed offspring
        parents_to_cross = parents_idx[:, : int(noff * self._p)]
        off_cross = self._do(pop[parents_to_cross])
        self.logger.debug("Crossed %d parents resulting in %d offsprings.",
                          parents_to_cross.size, off_cross.shape[0])

        # Not crossed offspring
        idx = parents_idx[:, int(noff * self._p):]
        shape_not_cross = (idx.shape[0] * idx.shape[1], pop.shape[1])
        off_not_cross = np.reshape(pop[idx], shape_not_cross)

        off = np.append(off_cross, off_not_cross, axis=0)[:noff, :]

        return off

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Specific procedure for each operator. To overriden method.

        Parameters
        ----------
        sub_pop: np.array
            3-D array. Set of individual acting like parents.
                1D: First or second parent.
                2D: Each parent.
                3D: Parent chromosome.

        Return
        ------
        np.array
            Set of crossed individuals.

        Raises
        ------
        NotImplementedError
            If method is not override.
        """
        raise NotImplementedError


class KPointCrossover(Crossover):
    """
    K-point crossover operator for evolutionary algorithms.

    This class implements the k-point crossover strategy, which selects
    k points at random along the parent individuals and swaps the
    corresponding segments between them to produce child offspring. The
    number of points (k) can be set by the user.

    Attributes
    ----------
    k: int
        The number of points to select for crossover.
    """

    def __init__(self, k: int = 2, p: float = 1) -> None:
        """
        Construct all necessary attributes for the K-Point crossover operator.

        Parameters
        ----------
        k: int
            Nº cut points.
        p: float
            Probability of a crossover occurring.
        """
        super().__init__(2, 2, p=p)
        self._k = k

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Apply the k-point crossover operator to the given parent individuals.

        This method selects k points at random along the parent individuals,
        swaps the corresponding segments between them, and returns the
        resulting child individual(s).

        Parameters
        ----------
        sub_pop: np.array
            3-D array. Individual acting like parents.
            1D: First or second parent.
            2D: Each parent.
            3D: Parent chromosome.

        Return
        ------
        np.array
            Array of resulting individuals.
        """
        possible_kpoints = np.arange(sub_pop.shape[2])
        kpoints = np.random.choice(possible_kpoints, self._k, replace=False)
        sorted_kpoints = np.sort(kpoints)

        for i in range(self._k):
            a = sub_pop[::-1, :, : sorted_kpoints[i]]
            sub_pop[:, :, : sorted_kpoints[i]] = a

        off_shape = (sub_pop.shape[0] * sub_pop.shape[1], sub_pop.shape[2])
        off = np.reshape(sub_pop, off_shape)

        return off

    def __str__(self):
        return f"{self.__class__.__name__}(k = {self._k}, p = {self._p})"


class UniformCrossover(Crossover):
    """
    Uniform crossover operator for evolutionary algorithms.

    This class implements the uniform crossover strategy, which randomly
    selects genes from either parent individual to create a child offspring.
    The gene is selected with equal probability from each parent.
    """

    def __init__(self, p: float = 1):
        """
        Construct all necesary attributes for the Uniform Crossover operator.

        Parameters
        ----------
        p: float
            Probability of a crossover ocurring.
        """
        super().__init__(2, 1, p=p)

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Apply the uniform crossover operator to the given parent individuals.

        This method selects genes randomly from either parent individual and
        combines them to create a child offspring. The gene is selected with
        equal probability from each parent.

        Parameters
        ----------
        sub_pop: np.array
            3-D array. Individual acting like parents.
            1D: First or second parent.
            2D: Each parent.
            3D: Parent chromosome.

        Return
        ------
        np.array
            Array of resulting individuals.

        References
        ----------
        [1] G. Syswerda, ‘Uniform Crossover in Genetic Algorithms’,
        presented at the International Conference on Genetic Algorithms,
        Jun. 1989.
        """
        idx = np.random.choice([0, 1], sub_pop[0].size)
        idx = np.reshape(idx, sub_pop[0].shape)

        off = np.where(idx, sub_pop[0], sub_pop[1])

        return off

    def __str__(self):
        return f"{self.__class__.__name__}(p = {self._p})"
