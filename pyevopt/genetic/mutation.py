"""
This module provides various mutation operators that are commonly used in
genetic algorithms.

These mutation operators can be used to introduce genetic variation into
the population, helping to maintain diversity and prevent premature
convergence.
"""
import logging
import numpy as np


class Mutation:
    """
    Abstract base class for all mutation operators in an evolutionary
    algorithm.

    This abstract class provides a general framework for creating new
    mutation operators. Users can subclass this class and implement their
    own specific mutation strategy to introduce genetic variation into
    the population.
    
    Attributes
    ----------
    p: float
        Probability of a mutation occurring.

    Notes:
        - This abstract class does not implement a specific mutation
        algorithm. Users are required to implement their own concrete
        subclass of this class that defines the specific mutation operator.
    """

    def __init__(self, p: float = 0.01) -> None:
        self._p = p
        self.logger = logging.getLogger(__name__)

    def do(self, pop: np.array) -> np.array:
        """
        Apply the mutation operator to the given individual with the given
        probability.

        This method should be implemented by subclasses of this class. It
        is expected to modify the input individual in place or return a
        new mutated individual.

        Parameters
        ----------
        pop: np.array
            All population.

        Return
        ------
        np.array
            Mutated population.
        """
        to_mutate = np.random.rand(pop.shape[0]) < self._p
        self.logger.debug("%d offsprings are selected to be mutated.",
                          sum(to_mutate))
        off = np.copy(pop)

        off[to_mutate, :] = self._do(off[to_mutate, :])

        return off

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Specific procedure for each operator. To overriden method.

        Parameters
        ----------
        sub_pop: np.array
            Set of individuals to be mutated.

        Return
        ------
        np.array
            Array of mutated individuals.
        """
        raise NotImplementedError


class FlipMutation(Mutation):
    """
    Class to represent a Flip Mutation operator.
    """

    def __init__(self, p: float = 0.01) -> None:
        super().__init__(p=p)

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Do Flip Mutation procedure.

        Parameters
        ----------
        sub_pop: np.array
            Set of individuals to be mutated.

        Return
        ------
        np.array
            Array of mutated individuals.
        """
        size, n_var = sub_pop.shape

        a = np.random.choice(np.arange(n_var), size, replace=True)
        b = (a + np.random.randint(1, n_var, size=size)) % n_var

        aux = sub_pop[np.arange(size), a]
        sub_pop[np.arange(size), a] = sub_pop[np.arange(size), b]
        sub_pop[np.arange(size), b] = aux

        return sub_pop

    def __str__(self):
        return f"{self.__class__.__name__}(p = {self._p})"


class NormalMutation(Mutation):
    """
    Normal distribution mutation operator.

    This mutation operator adds a random value from a normal distribution
    to each gene in the individual. The mean and standard deviation of the
    distribution are used to determine the scale of the mutation.

    Attributes
    ----------
    mu : float
        The mean of the normal distribution.
    sigma : float
        The standard deviation of the normal distribution.
    prob: float
        Probability of a mutation occurring.
    """

    def __init__(self, mu: float = 0.0, sigma: float = 0.1, p: float = 0.01):
        super().__init__(p=p)
        self._mu = mu
        self._sigma = sigma

    def _do(self, sub_pop: np.array) -> np.array:
        """
        Mutate individuals from `sub_pop` using this normal distribution
        mutation operator.

        Each gene in the individual is modified by adding a random value from
        a normal distribution. The mean and standard deviation of the
        distribution are determined by the `mu` and `sigma` attributes of 
        this class, respectively.
        
        Parameters
        ----------
        X: np.array
            Set of individuals to be mutated.

        Return
        ------
        np.array
            Array of mutated individuals.

        """
        off = sub_pop + np.random.normal(
            loc=self._mu, scale=self._sigma, size=sub_pop.shape
        )

        return off

    def __str__(self):
        return (f"{self.__class__.__name__}"
                + f"(mu = {self._mu}, sigma = {self._sigma},"
                + f" p = {self._p})")
