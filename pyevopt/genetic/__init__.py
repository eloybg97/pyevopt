"""
This module provides a collection of genetic algorithm implementations
and genetic operators.

The genetic algorithms in this module can be used to solve optimization
problems by iteratively applying selection, mutation, crossover and other
operations to a population of candidate solutions.

The genetic operators provided in this module include:

* Mutation: randomly changes the value of one or more genes in an individual.
* Crossover (also known as recombination): combines two parent individuals
  to produce a new offspring individual.
* Selection: chooses individuals from a population based on their fitness,
  which is typically defined by a problem-specific evaluation function.

This module provides a flexible framework for implementing genetic algorithms
and allows you to customize the algorithm to your specific optimization
problem.
"""
from . import crossover, selection, survival, mutation

__all__ = ["crossover", "selection", "survival", "mutation"]
