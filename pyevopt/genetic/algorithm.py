"""
This module provides a collection of genetic algorithm implementations.

These algorithms can be used to solve optimization problems by iteratively
applying selection, mutation, crossover and other operations to a population
of candidate solutions.
"""
from typing import Callable, Union
import numpy as np

from ..base import (Algorithm, Population)
from . import selection
from . import crossover
from . import mutation
from . import survival


class GeneticAlgorithm(Algorithm):
    """
    The `GeneticAlgorithm` class implements a generational genetic algorithm,
    which is a type of evolutionary algorithm that uses the principles of
    natural selection and genetics to solve optimization problems.

    This algorithm iteratively applies selection, crossover (recombination),
    mutation, and fitness evaluation to a population of candidate solutions.
    The goal is to find the optimal solution by evolving the population over
    multiple generations.

    Attributes
    ----------
        selection: genetic.selection.Selection
            Selection operator
        crossover: genetic.crossover.Crossover
            Crossover operator
        mutation: genetic.mutation.Mutation
            Mutation operator
        survival: genetic.survival.Survival
            Survival operator
    """
    def __init__(
        self,
        selection: selection.Selection,
        crossover: crossover.Crossover,
        mutation: mutation.Mutation,
        survival: survival.Survival,
    ):
        super().__init__()

        self._selection = selection
        self._crossover = crossover
        self._mutation = mutation
        self._survival = survival

    def next(self, population, objective):
        """
        Executes a single iteration of the genetic algorithm.

        This method applies selection, crossover (recombination), and
        mutation to the input population, and then evaluates the fitness
        of each individual in the new population using the provided
        `objective` function. The resulting population is then returned
        as the output of this method.

        Parameters
        ----------
        population: Population
            Represent the current generation of candidate solutions.

        objective: callable
            A callable function that evaluates the quality of each
            individual in the population.

        Returns
        -------
        pyevopt.base.Population:
            The new population of candidate solutions after a single
            iteration of the genetic algorithm.
        dict:
            A dictionary containing information about the current iteration,
            such as:
            
            * `evaluations`: The number of times the objective function
              was evaluated during this iteration.


        Notes
        -----
        This method is intended to be called repeatedly to iterate
        through the optimization process.
        - It is assumed that the `objective` function is deterministic,
        meaning it always returns the same output for a given input.

        """
        rel = self._crossover.input // self._crossover.output
        parents_idx = self._selection.do(
                population.population, population.fitness,
                to_select=population.n * rel)

        off = self._crossover.do(population.population, parents_idx)
        off = self._mutation.do(off)

        off = Population(off, population.domain)
        off.evaluate(objective)

        info = {
            "evaluations": off.n
        }

        return self._survival.do(population, off), info

    def __str__(self):
        return (f"{self.__class__.__name__}"
                + f"({self._selection}, {self._crossover}, {self._mutation})")
