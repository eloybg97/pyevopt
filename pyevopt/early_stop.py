"""
This module provides early stop criteria for evolutionary algorithms.

Provides various stopping conditions for evolutionary algorithms,
including score-based and generation-based termination strategies.

"""

from .base import EarlyStopCriteria


class MaxEvaluationCriteria(EarlyStopCriteria):
    """
    Termination criterion based on the maximum number of evaluations allowed.
    Maximum evaluation limit for an optimization run.
    """
    def __init__(self, max_evaluation: int = 60):
        super().__init__(max_evaluation, 0)

    def next(self, **kwargs) -> None:
        """
        Get the next evaluation limit based on the current number of
        evaluations.

        Parameters
        ----------
        evaluation: int
            Number of evaluations calculated in this iteration.
        """
        self.value = self.value + kwargs["evaluation"]

    def check(self) -> bool:
        """
        Check the condition to decide if a new iteration of the optimization
        process should be executed.

        Return
        ------
        bool:
            True if stop criteria is meet, False otherwise.
        """
        return self.max_threshold <= self.value

    def __str__(self):
        return f"{self.value} of {self.max_threshold} evaluations completed"


class MaxIterationCriteria(EarlyStopCriteria):
    """
    Termination criterion based on the maximum number of iterations allowed.
    Maximum iteration limit for an optimization run.
    """
    def __init__(self, max_iter: int = 60):
        super().__init__(max_iter, 0)

    def next(self, **kwargs):
        """
        Update iteration number in the optimization process.
        """
        self.value = self.value + 1

    def check(self) -> bool:
        """
        Check the condition to decide if continue the process or not.

        Return
        ------
        bool:
            True if  stop criteria is met, False otherwise.
        """
        return self.max_threshold <= self.value

    def __str__(self):
        return f"{self.value} of {self.max_threshold} generations completed"
