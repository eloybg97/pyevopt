import numpy as np
import logging.config

from pyevopt.optimizer import Optimizer

from pyevopt.sampler import RealSampler
from pyevopt.genetic.algorithm import GeneticAlgorithm
from pyevopt.genetic.selection import SelectionTournament
from pyevopt.genetic.crossover import UniformCrossover
from pyevopt.genetic.mutation import NormalMutation
from pyevopt.genetic.survival import BasicSurvival

from pyevopt.early_stop import MaxIterationCriteria


logging.config.fileConfig('samples/config/logging.conf')

def rastrin_function(X):
    return -np.sum(X * X - 10 * np.cos(2 * np.pi * X) + 10)

def main():
    N = 1
    selection = SelectionTournament(3)
    crossover = UniformCrossover(0.7)
    mutation = NormalMutation(0.01)
    survival = BasicSurvival()

    algorithm = GeneticAlgorithm(selection, crossover, mutation, survival)
    stop = MaxIterationCriteria(25)
    sampler = RealSampler()

    optimizer = Optimizer(algorithm)


    avg_fit = 0
    for _ in range(N):
        result = optimizer.run(rastrin_function, 10, (-100, 100), sampler, stop, pop_size = 60)

        best = result["best"]
        avg_fit = result["F"][best]

    avg_fit /= N
    print(f"Avg. Result: {avg_fit}")

    






if(__name__ == '__main__'):
    main()
