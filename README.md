# Pyevopt
[![pipeline status](https://gitlab.com/eloybg97/pyevopt/badges/main/pipeline.svg)](https://gitlab.com/eloybg97/pyevopt/-/commits/main)
[![coverage report](https://gitlab.com/eloybg97/pyevopt/badges/main/coverage.svg)](https://gitlab.com/eloybg97/pyevopt/-/commits/main) 
[![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/pylint-dev/pylint)


## Description
Our open-source framework pyevopt offers what is necessary to optimize real functions with genetics algorithms and a wide range of operators.

## Installation
To install the latest version follow this commands:
```
sudo apt-get install python3-venv python3-pip
python -m venv .env
source .env/bin/activate
pip install pyevopt --index-url https://gitlab.com/api/v4/projects/50659038/packages/pypi/simple
```

If you need specific version, type:
```
pip install pyevopt==<version> --index-url https://gitlab.com/api/v4/projects/50659038/packages/pypi/simple
```

## Support
If you need help using pyevopt or have detected any error, please open an issue to get in touch.

## Roadmap
pyevopt aims to be a repository with a wide variety of evolutionary algorithms: genetic algorithm, differential evolution, genetic programming, neuroevolution...

Next version will include differential evolution.

## Contributing
If you want to participate in pyevopt's implementation, you can make a pull request to the develop branch so that it will be reviewed and maybe integrated. The conditions to take your contribution are the followings:
- The branch passes all tests.
- All changes have its tests
- The changes introduced are related to the pyevopt's objectives.
- Obtain no warning with pycodestyle
- Obtain at least 9.00 with pylint
- Follow [this guide](https://numpydoc.readthedocs.io/en/latest/format.html) for docstrings

To start developing, please, follow the next steps:
```
sudo apt-get install python3-pip
pip install poetry
git clone git@gitlab.com:eloybg97/pyevopt.git
cd pyevopt
poetry install --with test,docs,code-style
```

Any suggestions you have for the pyevopt development will be welcome. Please, open an issue detailing your suggestion and it will be integrated as soon as possible.

## License
This open source project are licensed with MIT License.

## Project status
pyevopt is at an early stage of development. Being constant we will reach the goal.
